**Contact book application completely client side built with ReactJS**

This app can be used to add, delete and update contact details.
No data persistence layer is used so data will loss on each app restart.
The basic structure of this app is created by using create-react-app project from Facebook

---

## Src Folder

This folder contains all the necessary code of this application

---
